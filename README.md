# PanoLive

## PanoLive is an integration of [Panoramix](https://forum.ircam.fr/projects/detail/panoramix/) via Max for Live.

## *frozen devices* 

## Version 4.9 Use Panoramix 1.6.9 included in Spat 5.3.4

Here is an example of Live Set to help you find your way around, **the devices are inside.**

### For OSX and Windows (needs to install Spat on Windows)

#### **Changes :**

#### **Head Tracking OSC based on Panoramix for monitoring binaural movement.The sensors are accelerometers, gyroscopes, and magnetometers, based on Arduino : they are OSC bridges.**

### Version 4 :
> - Updated to Panoramix 1.6.9
> - Updated to Panoramix 1.6.6
> - Changed skin (dark mode)
> - Updated to Panoramix 1.6.5
> - Updated to Panoramix 1.6.4
> - Added Reverb presets
> - Added Snapshots
> - Updated to Panoramix 1.6.3
> - Added : **New sensor from [Feichter Audio](http://feichter-audio.com/produits/diffusion/t3/) The software for interface is T3 Utility or older, provided here.**
> - Added : **OSC for head movement. the sensor is an Arduino gyroscope made by [RJ Lab](https://www.facebook.com/HelloRJLab). the sotware for interface is the NVsonic Head Tracker OSC Bridge, provided here.**
> - Cleaning
> - Added : **PanoLiveControl : Automatic trajectories generator made by Alexis Baskind** and me.
> - Changed : Routing in Live (Thanks to Alexis Baskind)
> - Added : CPU optimization (Thanks to Alexis Baskind)
> - Adeed : Automation of the Panoramix session (Thanks to Alexis Baskind)
> - Added : New floating mode for all windows
> - Added : Presets speakers
> - UI improvement : Monitoring knobs endless, show faders on/off 


### Here it is, as well as a mapping file for [OSCar](https://forum.ircam.fr/projects/detail/oscar/) to manage [OSC](http://opensoundcontrol.org) messages, examples of Panoramix sessions, and a Live 11 project. 

#### PanoLive requires OSCar 1.2.5 (provided Here)

#### In Live, have a look at routing process ! In Panoramix, take a good look at the template session, you will see that it is set with a HOA 4th order bus rendering with a configuration of 24 speakers. Setup of Ircam studio 1 


#### About Templates :

> - Use PanoLiveControl instead of OSCar on a track
> - Made with Ableton Live 11.3 and 12.0 with Spat 5.3.2
> - Use OSCar in VST 3

#### About the sensors

The sensors needs to be calibrated often, you can do this in the device by pressing the "Reset" button but remember to reset it in the Head Tracker application sometimes.

> - See : https://github.com/trsonic/nvsonic-head-tracker be careful, the released app does not work. 
> - Try this application before buy the sensor. Contact Rémi Janot : [RJ Lab](https://www.facebook.com/HelloRJLab) if you have a trouble.
> - Attach it with rubber bands to the top of the headphones, the micro-usb plug is located on the left side of the sensor

> - See : http://feichter-audio.com/produits/diffusion/t3/ 
> - Try this application before buy the sensor. The drivers are all included
> - Attach it with rubber bands to the top of the headphones, the micro-usb plug is located on the left side of the sensor


#### About the RJ Lab Head tracker application
> - 1) Copy the [preset.xml] file in the new folder
> - 2) Copy [Head tracker OSC Bridge] in the new folder, you can rename the folder, then launch it
> - 3) Load PanoLive preset.

#### About the Feichter Audio T3 Head tracker application
> - See "how to set" picture

For help, Feedback, Professional use, and support, go to [discussion room](https://discussion.forum.ircam.fr/c/panolive)

The sensor is a bit experimental, do not hesitate to contact me…

Many thanks to Thibaut for his kindness.
